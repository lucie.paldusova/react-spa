import React, { Component } from 'react';
import logo from './logo.svg';
import './App.scss';
import Menu from './components/Menu';
import Footer from './components/Footer';
import Home from './pages/Home';
import Kontakt from './pages/Kontakt';
import NotFound from './pages/NotFound';
import Page from './pages/Page';
import { Route, Switch, Link } from "react-router-dom";

class App extends Component {
  render() {
    return (
        <div className="App">
          
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
          </header>

          <Menu />

          <Link to="/kontakt" className="kontakt-link">Kontakt</Link>
          
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/kontakt' component={Kontakt} />
            <Route path='/pages/:slug' component={Page} />
            <Route component={NotFound} />
          </Switch>

          <footer>
            <Footer />
          </footer>
        </div>
    );
  }
}

export default App;
