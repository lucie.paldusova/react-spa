import "./Menu.scss";
import React from "react";
import { Link } from "react-router-dom";

function template() {
  return (
    <div className="menu">
	    <ul>
	      {this.state.menu.map((item, i) => {
	        return (
	          <li key={item.link}>
	            <Link to={"/pages/"+item.link}>{i+1}. {item.name}</Link>
	            {item.sub != null ? (
	              <ul>
	                {item.sub.map((subitem, i) => {
	                  return (
	                    <li key={subitem.link}>
	                      <Link to={"/pages/"+subitem.link}>{subitem.name}</Link>
	                    </li>
	                  );
	                })}
	              </ul>
	            ) : null}
	          </li>
	        );
	      })}
	    </ul>
    </div>
  );
};

export default template;
