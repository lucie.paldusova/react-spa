import React    from "react";
import template from "./Menu.jsx";
import axios from 'axios';

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: []
    };
  }

  componentDidMount() {
    axios.get('/json/menu.json', {responseType: 'json'})
	  .then(res => {
        this.setState({ menu: res.data.menu });
      }).catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return template.call(this);
  }
}

export default Menu;
