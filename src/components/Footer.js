import React from "react";
import axios from 'axios';
import "./Footer.scss";

class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      footer: []
    };
  }

  componentDidMount() {
    axios.get('/json/footer.json', {responseType: 'json'})
	  .then(res => {
        this.setState({ footer: res.data.footer });
      }).catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div className="footer">
        <ul>
          {this.state.footer.map((item, i) => {
              return (
                <li key={item.link}>
                  <a href={item.link} target="_blank" rel="noopener noreferrer">{item.name}</a>
                </li>
              );
          })}
        </ul>
      </div>
    );
  }
}

export default Footer;
