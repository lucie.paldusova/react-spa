import "./NotFound.scss";
import React from "react";

function template() {
  return (
    <div className="not-found">
      <h1>NotFound</h1>
    </div>
  );
};

export default template;
