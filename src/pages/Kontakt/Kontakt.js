import React    from "react";
import template from "./Kontakt.jsx";

class Kontakt extends React.Component {
  render() {
    return template.call(this);
  }
}

export default Kontakt;
