import React from "react";
import template from "./Page.jsx";
import axios from 'axios';

class Page extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      article: null,
    };
    
  }

  getData() {
    axios.get('/json/pages.json', {responseType: 'json'})
      .then(res => {
        if(res.data[this.props.match.params.slug] !== undefined) {
          this.setState({ 
            article: res.data[this.props.match.params.slug],
          });
        } else {
          this.setState({ 
            article: null
          });
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  componentDidMount() {
    this.getData();
  }

  componentWillReceiveProps(props) {
    this.getData();
  }

  render() {
    return template.call(this);
  }
}

export default Page;
