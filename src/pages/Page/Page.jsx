import "./Page.scss";
import React from "react";
import NotFound from'../NotFound';

function template() {
  return (this.state.article != null ? (
  		<div className="page">
  			<div className="container">
		      	<h1>{this.state.article.name}</h1>
		      	<div className="perex"><b>{this.state.article.perex}</b></div>
		      	<br />
		       <div className="text">{this.state.article.text}</div>
		    </div>
	    </div>
  	) : (
  		<NotFound />
  	)
  );
};

export default template;
